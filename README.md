# Virgotchi

/ ! \ Not finished yet. A Tamagotchi-like project, with an user profile (CSS-customiseable)

## Config & requirements :
* php7.2
* mariaDB
* micro-framework : Slim
* node (for npm)
* composer

## How to test this project
1. Clone this project

2. Install needed packages :
```
composer update
npm run dev
composer dump-autoload
```

3. Use src/db files to create a DB with random datas (you'll need to create also an SQL user)

4. Duplicate (or rename) config/config-example.php file and complete its parameters