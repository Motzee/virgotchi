const webpack = require("webpack");
const path = require('path');

let config = {
  entry: {
  	index: "./src/js/index.js",
  	admin: "./src/js/admin.js"
  },
  output: {
    path: path.resolve(__dirname, "./public/js"),
    filename: "./[name].js"
  }
} ;

module.exports = config ;


