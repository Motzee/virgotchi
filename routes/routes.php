<?php

//rappel : les controllers appelés ici sont introduits par le container gérant les controllers

use Middlewares\Auth ;
use Middlewares\AuthAdmin ;

/* Les pages navigables */

    //Public
$app->get('/', 'NavigationController:index')->setName('index') ;
$app->get('/informations[/]', 'NavigationController:infosLegales')->setName('infosLegales') ;
$app->get('/profil/{slug:[a-zA-Z0-9_-]+}[/]', 'NavigationController:profilVoir')->setName('profilVoir') ;
$app->get('/resume[/]', 'NavigationController:resume')->setName('resume') ;
$app->get('/erreur[/]', 'NavigationController:errorPage')->setName('errorpage') ;

    //Membres connectés
$app->get('/tchi[/]', 'NavigationController:tchi')->setName('tchi')->add(new Auth()) ;
$app->get('/profil[/]', 'NavigationController:profilPersonnaliser')->setName('profilPersonnaliser')->add(new Auth()) ;
$app->get('/compte[/]', 'NavigationController:compteParametrer')->setName('compteParametrer')->add(new Auth()) ;

    //Administrateurs
$app->get('/administration[/]', 'NavigationController:administration')->setName('administration')->add(new AuthAdmin()) ;


/* Les pages de traitement de requêtes */
    //authentification
$app->post('/log', 'TraitementController:log') ;
$app->post('/deconnexion', 'NavigationController:deconnexion')->setName('deconnexion') ;

    //compte
$app->put('/compte', 'TraitementController:updateCompte')->add(new Auth()) ;
$app->delete('/compte', 'TraitementController:deleteCompte')->add(new Auth()) ;

    //profil
$app->post('/profil', 'TraitementController:postProfil')->add(new Auth()) ;

    //tchi
$app->post('/tchi', 'TraitementController:createTchi')->add(new Auth()) ;


/* API */

$app->group('/api', function() use ($app) {
    $app->get('/tchi[/]', 'ApiController:readTchi') ;
    $app->get('/stats[/]', 'ApiController:readStats') ;
    $app->put('/stats', 'ApiController:updateStats') ;
})->add(new Auth()) ;

    //partie admin
$app->group('/admin/api', function() use ($app) {
    $app->get('/membres[/]', 'ApiAdminController:allMembres') ;
    $app->get('/tchis[/]', 'ApiAdminController:allTchis') ;
})->add(new AuthAdmin()) ;
