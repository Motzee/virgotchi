<?php

session_cache_limiter(false);
session_start();
//setcookie(session_name(),session_id(),time()+3600, null, null, true, true); //expire after 1 hour of inactivity

require __DIR__ . '/../vendor/autoload.php';

$config = require __DIR__ . '/../config/config.php';
$app = new Slim\App($config);

$container = require __DIR__ . '/../containers/call_containers.php';

require __DIR__ . '/../routes/routes.php';

$app->run();