/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/js/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/index.js":
/*!*************************!*\
  !*** ./src/js/index.js ***!
  \*************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _paquets_navigation__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./paquets/navigation */ \"./src/js/paquets/navigation.js\");\n\n\n/* Déclaration du service worker si option disponible*/\nif (\"serviceWorker\" in navigator) {\n    navigator.serviceWorker\n      .register(\"/service-worker.js\")\n      .then(registration => {\n        console.log(\n          \"App: Achievement unlocked.\"\n        );\n      })\n      .catch(error => {\n        console.error(\n          \"App: Crash of Service Worker\",\n          error\n        );\n      });\n  }\n\n/* Menu de navigation */\nObject(_paquets_navigation__WEBPACK_IMPORTED_MODULE_0__[\"menuNavigation\"])() ;\n\nlet pageScript = {} ;\n\npageScript[\"pageTchi\"] = _paquets_navigation__WEBPACK_IMPORTED_MODULE_0__[\"affichageTchi\"] ;\npageScript[\"pageProfil\"] = function() {} ;\npageScript[\"pageProfilVoir\"] = function() {} ;\npageScript[\"pageIndex\"] = _paquets_navigation__WEBPACK_IMPORTED_MODULE_0__[\"indexSite\"] ;\npageScript[\"pageParametres\"] = _paquets_navigation__WEBPACK_IMPORTED_MODULE_0__[\"parametrageCompte\"] ;\npageScript[\"pageInfoslegales\"] = function() {} ;\n\n\nlet pagesDuSite = [\n    \"pageTchi\",\n    /*\"pageProfil\",\n    \"pageProfilVoir\",*/\n    \"pageIndex\",\n    \"pageParametres\",\n    //\"pageInfoslegales\"\n] ;\n\nfor(let aPage in pagesDuSite) {\n    if(document.getElementById(pagesDuSite[aPage]) !== null) {\n        pageScript[pagesDuSite[aPage]]() ;\n        break ;\n    }\n}\n\n//# sourceURL=webpack:///./src/js/index.js?");

/***/ }),

/***/ "./src/js/paquets/ajaxMultisurfaces.js":
/*!*********************************************!*\
  !*** ./src/js/paquets/ajaxMultisurfaces.js ***!
  \*********************************************/
/*! exports provided: ajaxMultisurfaces */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ajaxMultisurfaces\", function() { return ajaxMultisurfaces; });\n//Méthode GET ou POST, url, asynchrone ou pas, arguments, fonction callback, fonction callback si erreur\nfunction ajaxMultisurfaces(options) {\n    //on trouvera souvent ce type de fonction appelée XHR pour XML Http Request)\n    let defaultOptions = {\n        method: 'GET',\n        url: '',\n        async: true,\n        args: '',\n        callback: function() {},\n        callbackError: function() {}\n    } ;\n\n    assignArgs(options, defaultOptions);\n\n    let monAjax = new XMLHttpRequest();\n\n    //Comme c'est asynchrone, on n'a pas la réponse là tout de suite : il faut préparer l'équivalent d'un addeventlistener ajaxien :\n    monAjax.onreadystatechange = function() {\n\n        if (monAjax.readyState === 4) {\n            //conditionnelle pour ne prendre que les requêtes réussies (ou \"not modified\")\n            if (monAjax.status === 200 || monAjax.status === 304) {\n                defaultOptions.callback(JSON.parse(monAjax.response));\n            } else {\n                defaultOptions.callbackError();\n            }\n        }\n    };\n\n    if(defaultOptions.method === 'GET') {\n        //on précise la méthode, le chemin, et si c'est asynchrone ou pas\n        monAjax.open(defaultOptions.method, defaultOptions.url+defaultOptions.args, defaultOptions.async);\n        //on envoie la requête\n        monAjax.send();\n    } else if(defaultOptions.method === 'POST') {\n        monAjax.open(defaultOptions.method, defaultOptions.url, defaultOptions.async);\n        monAjax.setRequestHeader(\"Content-Type\", \"application/x-www-form-urlencoded\");\n        monAjax.send(defaultOptions.args);\n    } else {\n        monAjax.open(defaultOptions.method, defaultOptions.url, defaultOptions.async);\n        monAjax.send(defaultOptions.args);\n    }\n\n\n    function assignArgs(source, target) {\n        for (let clef in source) {\n            if (target.hasOwnProperty(clef)) {\n                target[clef] = source[clef];\n            }\n        }\n    }\n\n}\n\n//# sourceURL=webpack:///./src/js/paquets/ajaxMultisurfaces.js?");

/***/ }),

/***/ "./src/js/paquets/navigation.js":
/*!**************************************!*\
  !*** ./src/js/paquets/navigation.js ***!
  \**************************************/
/*! exports provided: indexSite, menuNavigation, parametrageCompte, affichageTchi */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"indexSite\", function() { return indexSite; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"menuNavigation\", function() { return menuNavigation; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"parametrageCompte\", function() { return parametrageCompte; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"affichageTchi\", function() { return affichageTchi; });\n/* harmony import */ var _ajaxMultisurfaces__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ajaxMultisurfaces */ \"./src/js/paquets/ajaxMultisurfaces.js\");\n/* harmony import */ var _tchi__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./tchi */ \"./src/js/paquets/tchi.js\");\n\n\n\nfunction indexSite() {\n    let authForm = document.getElementById(\"authForm\") ;\n    if(typeof authForm !== 'undefined' && authForm !== null) {\n        authForm.addEventListener(\"submit\", function(e){\n            \n            let pseudo = document.getElementById(\"pseudo\").value ;\n\n            if (/^[a-zA-Z0-9_-]{2,32}$/.test(pseudo) === false) {\n                e.preventDefault();\n                alert(\"Votre pseudo ne peut contenir que des lettres majuscules ou minuscules, des chiffres ou des tirets - ou _\");\n            }\n        }, false) ;\n    }\n}\n\nfunction menuNavigation() {\n   \n    let btnOpenMenuNav = document.getElementById(\"btnMenuNav\") ;\n    \n    if(typeof btnOpenMenuNav !== 'undefined' && btnOpenMenuNav !== null) {\n\n        let modaleMenuNav = document.getElementById(\"modaleMenuNav\") ;\n\n        btnOpenMenuNav.addEventListener(\"click\", function() {\n            modaleMenuNav.classList.remove(\"hiddenModale\");\n        }) ;\n\t        \n        let btnCloseMenuNav = document.getElementById(\"closeModale\") ;\n        btnCloseMenuNav.addEventListener(\"click\", function() {\n            modaleMenuNav.classList.add(\"hiddenModale\");\n        }) ;\n    }\n        \n}\n\nfunction parametrageCompte() {\n\tlet champNewPasswd = document.getElementById(\"champNewPasswd\") ;\n\n\tlet caseModifierPass = document.getElementById(\"modifierPass\") ;\n\tcaseModifierPass.addEventListener(\"click\", function() {\n\t    champNewPasswd.classList.toggle(\"hidden\");\n\t}) ;\n\n\tlet actionDelete = document.getElementById(\"actionDelete\") ;\n\n\tlet btnRelacherTchi = document.getElementById(\"relacherTchi\") ;\n\tbtnRelacherTchi.addEventListener(\"click\", function(event) {\n\t    actionDelete.value = \"relacherTchi\" ;\n\t}) ;\n\n\tlet btnSupprimerCompte = document.getElementById(\"supprimerCompte\") ;\n\tbtnSupprimerCompte.addEventListener(\"click\", function(event) {\n\t    actionDelete.value = \"supprimerCompte\" ;\n\t}) ;\n}\n\nfunction affichageTchi() {\n\n\tObject(_ajaxMultisurfaces__WEBPACK_IMPORTED_MODULE_0__[\"ajaxMultisurfaces\"])({\n        url: '/api/stats',\n        callback: _tchi__WEBPACK_IMPORTED_MODULE_1__[\"updateBarresStats\"]\n    }) ;\n    \n    let btnsInteractionTchi = [\n        \"btnNourrir\",\n        \"btnBorder\",\n        \"btnToiletter\",\n        \"btnAmuser\"\n    ] ;\n    \n    for(let aBtn in btnsInteractionTchi) {\n        let csrf_name = document.getElementsByName(\"csrf_name\")[0].value ;\n        let csrf_value = document.getElementsByName(\"csrf_value\")[0].value ;\n        \n        let btn = document.getElementById(btnsInteractionTchi[aBtn]) ;\n        btn.addEventListener(\"click\", function() {\n        \t\n        \tlet argumnts = \"_METHOD=PUT&action=\"+btnsInteractionTchi[aBtn]+\"&csrf_name=\"+csrf_name+\"&csrf_value=\"+csrf_value ;\n\n        \tObject(_ajaxMultisurfaces__WEBPACK_IMPORTED_MODULE_0__[\"ajaxMultisurfaces\"])({\n\t\t        method: 'POST',\n\t\t        url: '/api/stats',\n\t\t        args: argumnts,\n\t\t        callback: _tchi__WEBPACK_IMPORTED_MODULE_1__[\"updateBarresStats\"]\n\t\t    }) ;\n        }) ;\n    }\n\n}\n\n//requête ajax\nfunction ajaxRequest(reqType, url, actionKey, data = null) {\n    let authorizedReqType = [\"GET\", \"POST\"] ;\n    \n    //vérifications\n    if(authorizedReqType.indexOf(reqType) === -1) {\n        console.log(\"Method request not authorized\") ;\n    }\n    \n    let xhr = new XMLHttpRequest();\n    xhr.onreadystatechange = function() {\n        if (this.readyState == 4 && this.status == 200) {\n            data = JSON.parse(this.response, true) ;\n            if(data.errors === 0) {\n                actionsApresReqAjax[actionKey](data) ;\n            } else {\n                console.log(\"Le back renvoie une erreur !\") ;\n            }\n        }\n    };\n    xhr.open(reqType, url, true);\n    if(data !== null) {\n        xhr.send(data);\n    } else {\n        xhr.send();\n    }\n}\n\n//# sourceURL=webpack:///./src/js/paquets/navigation.js?");

/***/ }),

/***/ "./src/js/paquets/tchi.js":
/*!********************************!*\
  !*** ./src/js/paquets/tchi.js ***!
  \********************************/
/*! exports provided: updateBarresStats */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"updateBarresStats\", function() { return updateBarresStats; });\nfunction updateBarresStats(newStats) {\n\n    //tableau associant chaque champ reçu en ajax avec l'id de la barre à remplir avec\n    let idsBarres = {\n        \"satiete\" : \"statSatiete\",\n        \"repos\" : \"statRepos\",\n        \"proprete\" : \"statProprete\",\n        \"distraction\" : \"statDistraction\"\n    } ;\n    \n    let values = newStats.values ;\n\n    for(let aStat in values) {\n        let barreValue = values[aStat] * 2.5 ;\n        let barre = document.getElementById(idsBarres[aStat]) ;\n        barre.style.backgroundImage = \"linear-gradient(to right, grey, grey \" + barreValue + \"px, white \" + barreValue + \"px, white)\" ;\n    }\n}\n    \n\n//# sourceURL=webpack:///./src/js/paquets/tchi.js?");

/***/ })

/******/ });