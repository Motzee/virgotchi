/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/js/admin.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/admin.js":
/*!*************************!*\
  !*** ./src/js/admin.js ***!
  \*************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _paquets_ajaxMultisurfaces__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./paquets/ajaxMultisurfaces */ \"./src/js/paquets/ajaxMultisurfaces.js\");\n\n\nif(document.getElementById(\"pageAdministration\") !== null) {\n    \n    var actionsApresReqAjax = {} ;\n\n    actionsApresReqAjax[\"adminMembres\"] = function(data) {\n        remplirTabMembres(\"membresTbody\", data);\n    } ;\n\n    actionsApresReqAjax[\"adminTchis\"] = function(data) {\n        remplirTabTchis(\"tchisTbody\", data) ;\n    } ;\n\n    let zones = [\"administrerMembres\", \"administrerTchis\"] ;\n\n\n    Object(_paquets_ajaxMultisurfaces__WEBPACK_IMPORTED_MODULE_0__[\"ajaxMultisurfaces\"])({\n        url: '/admin/api/membres',\n        callback: actionsApresReqAjax[\"adminMembres\"]\n    }) ;\n\n\n    let actionAdminMembres = document.getElementById(\"actionAdminMembres\") ;\n    actionAdminMembres.addEventListener('click', function() {\n        Object(_paquets_ajaxMultisurfaces__WEBPACK_IMPORTED_MODULE_0__[\"ajaxMultisurfaces\"])({\n            url: '/admin/api/membres',\n            callback: actionsApresReqAjax[\"adminMembres\"]\n        }) ;\n        montrerZone(\"administrerMembres\", zones) ;\n    }) ;\n\n    let actionAdminTchis = document.getElementById(\"actionAdminTchis\") ;\n    actionAdminTchis.addEventListener('click', function() {\n        Object(_paquets_ajaxMultisurfaces__WEBPACK_IMPORTED_MODULE_0__[\"ajaxMultisurfaces\"])({\n            url: '/admin/api/tchis',\n            callback: actionsApresReqAjax[\"adminTchis\"]\n        }) ;\n        montrerZone(\"administrerTchis\", zones) ;\n    }) ;\n\n}\n\nfunction remplirTabMembres(idTableau, data) {\n        let tableau = document.getElementById(idTableau) ;\n        tableau.textContent = \"\" ;\n        for(let entree in data.values) {\n            let ligne = document.createElement(\"tr\");\n            let lienMembre = document.createElement(\"a\");\n            let cellulePseudo = document.createElement(\"td\");\n            lienMembre.textContent = data.values[entree][\"pseudo\"] ;\n            lienMembre.href = \"/profil/\"+data.values[entree][\"slug\"] ;\n            cellulePseudo.appendChild(lienMembre) ;\n            ligne.appendChild(cellulePseudo) ;\n\n            creerElement(\"td\", data.values[entree][\"derniereco\"], ligne) ;\n            creerElement(\"td\", data.values[entree][\"active\"], ligne) ;\n            tableau.appendChild(ligne);\n        }\n        let nbResults = document.getElementById(\"nbMembres\") ;\n        nbResults.textContent = data.nbValues ;\n}\n\n    \nfunction remplirTabTchis(idTableau, data) {\n        let tableau = document.getElementById(idTableau) ;\n        tableau.textContent = \"\" ;\n        for(let entree in data.values) {\n            let ligne = document.createElement(\"tr\");\n            creerElement(\"td\", data.values[entree][\"nomTchi\"], ligne) ;\n\n            let lienMembre = document.createElement(\"a\");\n            let cellulePseudo = document.createElement(\"td\");\n            lienMembre.textContent = data.values[entree][\"pseudo\"] ;\n            lienMembre.href = \"/profil/\"+data.values[entree][\"slugPseudo\"] ;\n            cellulePseudo.appendChild(lienMembre) ;\n            ligne.appendChild(cellulePseudo) ;\n\n            creerElement(\"td\", data.values[entree][\"nbJours\"], ligne) ;\n            creerElement(\"td\", data.values[entree][\"active\"], ligne) ;\n            tableau.appendChild(ligne);\n        }\n        let nbResults = document.getElementById(\"nbTchis\") ;\n        nbResults.textContent = data.nbValues ;\n}\n\nfunction creerElement(elementHtml, txt, parent) {\n        let element = document.createElement(elementHtml);\n        element.textContent = txt;\n        parent.appendChild(element);\n}\n\n\nfunction montrerZone(idZone, listeZones) {\n        for(let aZone in listeZones) {\n            document.getElementById(listeZones[aZone]).classList.add(\"hidden\");\n        }\n        document.getElementById(idZone).classList.remove(\"hidden\");\n}\n\n\n//# sourceURL=webpack:///./src/js/admin.js?");

/***/ }),

/***/ "./src/js/paquets/ajaxMultisurfaces.js":
/*!*********************************************!*\
  !*** ./src/js/paquets/ajaxMultisurfaces.js ***!
  \*********************************************/
/*! exports provided: ajaxMultisurfaces */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ajaxMultisurfaces\", function() { return ajaxMultisurfaces; });\n//Méthode GET ou POST, url, asynchrone ou pas, arguments, fonction callback, fonction callback si erreur\nfunction ajaxMultisurfaces(options) {\n    //on trouvera souvent ce type de fonction appelée XHR pour XML Http Request)\n    let defaultOptions = {\n        method: 'GET',\n        url: '',\n        async: true,\n        args: '',\n        callback: function() {},\n        callbackError: function() {}\n    } ;\n\n    assignArgs(options, defaultOptions);\n\n    let monAjax = new XMLHttpRequest();\n\n    //Comme c'est asynchrone, on n'a pas la réponse là tout de suite : il faut préparer l'équivalent d'un addeventlistener ajaxien :\n    monAjax.onreadystatechange = function() {\n\n        if (monAjax.readyState === 4) {\n            //conditionnelle pour ne prendre que les requêtes réussies (ou \"not modified\")\n            if (monAjax.status === 200 || monAjax.status === 304) {\n                defaultOptions.callback(JSON.parse(monAjax.response));\n            } else {\n                defaultOptions.callbackError();\n            }\n        }\n    };\n\n    if(defaultOptions.method === 'GET') {\n        //on précise la méthode, le chemin, et si c'est asynchrone ou pas\n        monAjax.open(defaultOptions.method, defaultOptions.url+defaultOptions.args, defaultOptions.async);\n        //on envoie la requête\n        monAjax.send();\n    } else if(defaultOptions.method === 'POST') {\n        monAjax.open(defaultOptions.method, defaultOptions.url, defaultOptions.async);\n        monAjax.setRequestHeader(\"Content-Type\", \"application/x-www-form-urlencoded\");\n        monAjax.send(defaultOptions.args);\n    } else {\n        monAjax.open(defaultOptions.method, defaultOptions.url, defaultOptions.async);\n        monAjax.send(defaultOptions.args);\n    }\n\n\n    function assignArgs(source, target) {\n        for (let clef in source) {\n            if (target.hasOwnProperty(clef)) {\n                target[clef] = source[clef];\n            }\n        }\n    }\n\n}\n\n//# sourceURL=webpack:///./src/js/paquets/ajaxMultisurfaces.js?");

/***/ })

/******/ });