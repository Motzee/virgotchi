<?php

return [
    'settings' => [
        'displayErrorDetails' => true,
        'twigCache' => false,
        'twigCachePath' => '../cache/twig/',
        'db' => [
           'driver' => 'mysql',
           'host' => 'localhost',
           'database' => 'nomDB',
           'username' => 'nomUser',
           'password' => 'mdpUser',
           'charset' => 'utf8',
           'collation' => 'utf8_unicode_ci',
           //'prefix' => ''
        ]
    ]
] ;
