<?php

namespace Middlewares ;

use \Slim\Http\Request as Request;
use \Slim\Http\Response as Response;

class Auth {

    public function __invoke(Request $request, Response $response, $next) {
        
        if(!isset($_SESSION["slug"])) {
            //throw new \Exception("Unauthorized trying access.") ;
            header("Location: /");
            die();
        }
        
        $membre = \Models\Membre::where('slug', $_SESSION["slug"])->first();
        
        $membreRoles = $membre->recupRolesSlugs() ;
        
        if(!in_array("MEMBRE", $membreRoles)) {
            //throw new \Exception("Unauthorized trying access.") ;
            header("Location: /");
            die();
        } else {
        
        $response = $next($request, $response);

        return $response;
        }
    }
}