<?php

namespace Middlewares ;

use \Slim\Http\Request as Request;
use \Slim\Http\Response as Response;

class AuthAdmin {
    public function __invoke(Request $request, Response $response, $next) {
        
        if(!isset($_SESSION["slug"])) {
            throw new \Exception("Unauthorized", 401) ;
        }
        
        $membre = \Models\Membre::where('slug', $_SESSION["slug"])->first();
        
        $membreRoles = $membre->recupRolesSlugs() ;
        
        if(in_array("ADMIN", $membreRoles)) {
            $response = $next($request, $response);

            return $response;
        } else {
            throw new \Exception("Unauthorized", 401) ;
        }
    }
}