<?php

namespace Core;

class FieldsForms {
    
    const TEXT = 65535 ;

    const TCHI_CREATE_FORM = [
        "nomTchi" => [
            "type" => "string",
            "canBeNullOrEmpty" => false,
            "minLength" => 1,
            "maxLength" => 64,
            "sanitizeTags" => true
        ]
    ] ;
    
    const LOG_FORM = [
        "pseudo" => [
            "type" => "string",
            "canBeNullOrEmpty" => false,
            "minLength" => 2,
            "maxLength" => 32,
            "sanitizeTags" => true
        ],
        "password" => [
            "type" => "string",
            "canBeNullOrEmpty" => false,
            "minLength" => 6,
            "maxLength" => 255,
            "sanitizeTags" => true
        ],
        "logtype" => [
            "type" => "string",
            "whitelist" => ["connexion", "inscription"]
        ]
    ];

    const COMPTE_UPDATE_FORM = [
        "email" => [
            "type" => "string",
            "canBeNullOrEmpty" => true,
            "minLength" => 6,
            "maxLength" => 255,
            "sanitizeTags" => true
        ],
        "passwd" => [
            "type" => "string",
            "canBeNullOrEmpty" => false,
            "minLength" => 6,
            "maxLength" => 255,
            "sanitizeTags" => false
        ],
        "new_passwd" => [
            "type" => "string",
            "canBeNullOrEmpty" => true,
            "minLength" => 6,
            "maxLength" => 255,
            "sanitizeTags" => false
        ],
        "modifierPass" => [
            "type" => "checkbox"
        ]
    ] ;
    
    const COMPTE_DELETE_FORM = [
        "actionDelete" => [
            "type" => "string",
            "whitelist" => ["relacherTchi", "supprimerCompte"]
        ],
        "modifierIrreversible" => [
            "type" => "checkbox"
        ]
    ] ;
    
    const PROFIL_FORM = [
        "descriptionTchi" => [
            "type" => "string",
            "canBeNullOrEmpty" => true,
            "minLength" => null,
            "maxLength" => self::TEXT,
            "sanitizeTags" => true
        ],
        "descriptionMembre" => [
            "type" => "string",
            "canBeNullOrEmpty" => true,
            "minLength" => null,
            "maxLength" => self::TEXT,
            "sanitizeTags" => true
        ],
        "miseEnFormeCss" => [
            "type" => "string",
            "canBeNullOrEmpty" => true,
            "minLength" => null,
            "maxLength" => self::TEXT,
            "sanitizeTags" => true
        ],
    ] ;
    

}
