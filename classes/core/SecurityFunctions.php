<?php

namespace Core;

class SecurityFunctions {

    static function genererSlug(string $chaine, bool $suffixe = false):string {
        $slug = strtolower($chaine) ;
        if($suffixe === true) {
            //TODO concat sécurisée
            $slug .= self::genererSuffixe() ;
        }
        return $slug ;
    }

    static function genererSuffixe():string {
        return '_'.bin2hex(random_bytes(10)) ;
    }
    
    static function checkString($fieldReceived, array $expectedParams):string {
        
        if(isset($expectedParams["whitelist"])) {
            //cas de liste blanche
            if(in_array($fieldReceived, $expectedParams["whitelist"])) {
                return $fieldReceived ;
            } else {
                throw new \Exception('An expected string don\'t match with listing of authorized values');
            }
        } else {
            //vérifier le type
            if (!is_string($fieldReceived)) {
                throw new \Exception("An expected string isn't a string, that's embarassing...");
            }
            //vérifier que ça a le droit éventuellement d'être null ou vide
            if ($expectedParams["canBeNullOrEmpty"] === false && (trim($fieldReceived) === "" || is_null($fieldReceived))) {
                throw new \Exception('An expected string is empty');
            }
            
            //
            if(!is_null($expectedParams["maxLength"]) && strlen($fieldReceived) > $expectedParams["maxLength"]) {
                throw new \Exception('An expected string is too long');
            }
            if ($expectedParams["canBeNullOrEmpty"] === false && (is_null($expectedParams["minLength"]) === false && strlen($fieldReceived) < $expectedParams["minLength"])) {
                throw new \Exception('An expected string is too short');
            }
            
            //nettoyage éventuel des balises insérées
            if ($expectedParams["sanitizeTags"] !== false) {
                return $fieldReceived;
            } else {
                return filter_var($fieldReceived, FILTER_SANITIZE_STRING);
            }
        }
    }

}
