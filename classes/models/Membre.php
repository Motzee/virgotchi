<?php

namespace Models ;
 
use Illuminate\Database\Eloquent\Model ;

class Membre extends Model {
    /**
     * Table associated with the model
     * @var string
     */
    protected $table = 'membres';
    
    /**
     * Indicates if the model should be timestamped automatically
     * @var bool
     */
    public $timestamps = true;
    
    /**
     * Indicates the name of the fields in DB for automatic timestamps
     * @var bool
     */
    const CREATED_AT = 'date_inscript';
    const UPDATED_AT = 'date_derniereco';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ["email", "passhash", "date_derniereco", "nb_jours_jeu", "active"] ;
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['passhash'];
   
    /* N-to-M*/
    public function roles() {
        return $this->belongsToMany('Models\Role', 'role_membre', 'id_membre', 'id_role');
    }
    
    /* One to Many */
    public function tchis() {
        return $this->hasMany('Models\Tchi', 'id_membre');
    }
    
    /* One to Many */
    public function descriptions() {
        return $this->hasMany('Models\Description', 'id_membre');
    }

    
/* --------------------Méthodes CRUD+ ----------------------------- */
    
    static function creerMembre(string $pseudo, string $passhash):Membre {
        //création du membre
        $membre = new Membre ;
        
        $membre->pseudo = $pseudo ;
        $membre->slug = \Core\SecurityFunctions::genererSlug($pseudo) ;
        $membre->passhash = $passhash ;
        $membre->save();
           
        //attribution du role MEMBRE par défaut
        $roleMEMBRE = Role::where('slug', 'MEMBRE')->first() ;
        $membre->roles()->attach($roleMEMBRE->id) ;
        
        return $membre ;
    }
    
    static function existeMembre(string $slug):bool {
        if(Membre::where('slug', $slug)->count() > 0) {
            return true ;
        } else {
            return false ;
        }
    }
    
    public function modifierMembre(string $email, $passhash = null) {
        if(!is_null($passhash)) {
            $this->passhash = $passhash ;
        }
        $this->email = $email ;
        $this->save() ;
    }
    
/* --------------------Méthodes persos ----------------------------- */
    
    public function desactiver() {
        if(!is_null($this)) {
            //désactiver description
            if(!is_null($this->recupDescription())) {
                $this->recupDescription()->desactiver() ;
            }

            //désactiver Tchi et effacer ses stats
            if(!is_null($this->recupTchi())) {
                $this->recupTchi()->desactiver() ;
            }

            //désactiver le membre
            $this->update(['active' => 0]) ;
        }
    }
    
    public function recupTchi() {
        return Tchi::where('id_membre', $this->id)
            ->where('active', 1)
            ->first() ;
    }
    
    public function recupRolesSlugs():array {
        $rolesSlugs = [] ;
        foreach($this->roles as $unRoleObj) {
            $rolesSlugs[] = $unRoleObj->slug ;
        }
        return $rolesSlugs ;
    }
    
    public function recupRolesNoms():array {
        $rolesNoms = [] ;
        foreach($this->roles as $unRoleObj) {
            $rolesNoms[] = $unRoleObj->nom ;
        }
        return $rolesNoms ;
    }
    
    public function recupDescription() {
        return Description::where('id_membre', $this->id)
            ->where('active', 1)
            ->first() ;
    }
    
    public function connexion() {
        $_SESSION['slug'] = $this->slug ;
        
        //est-ce que c'est la première connexion quotidienne ?
        if($this->date_derniereco->format('Y-m-d') !== date('Y-m-d')) {
            $this->update([
                'nb_jours_jeu' => $this->nb_jours_jeu + 1,
                'date_derniereco' => date("Y-m-d H:i:s")
            ]) ;
        } else {
            $this->update([
                'date_derniereco' => date("Y-m-d H:i:s")
            ]) ;
        }
    }
    
    
    public function deconnexion() {
        $_SESSION = array();

        session_destroy();
    }
}
