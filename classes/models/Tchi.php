<?php

namespace Models ;
 
use Illuminate\Database\Eloquent\Model ;

use \Models\TchiStats ;

class Tchi extends Model {
    /**
     * Table associated with the model
     * @var string
     */
    protected $table = 'tchis';
    
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = ["nb_jours_jeu", "active"] ;
    
    /**
     * Indicates if the model should be timestamped automatically
     * @var bool
     */
    public $timestamps = false;
    
    /* Many to One */
    public function membre() {
        return $this->belongsTo('Models\Membre', 'id_membre');
    }
    
    /* One to One */
    public function stats() {
        return $this->hasOne('Models\TchiStats', 'id_tchi');
    }
    
    
/* --------------------Méthodes CRUD+ ----------------------------- */
    
    static function creerTchi(string $nom, int $idMembre):Tchi {
        //créer le Tchi
        $tchi = new Tchi ;
        
        $tchi->nom = $nom ;
        $tchi->slug = \Core\SecurityFunctions::genererSlug($nom, true) ;
        var_dump($tchi->slug) ;
        $tchi->id_membre = $idMembre ;
                
        $tchi->save();
        
        //création de sa jauge
        $stats = TchiStats::creerStats($tchi) ;
        
        return $tchi ;
    }
    
    static function existeTchi(string $slug):bool {
        if(Tchi::where('slug', $slug)->count() > 0) {
            return true ;
        } else {
            return false ;
        }
    }
    
    //récupérer un Tchi activé
    static function lireTchi(string $slug):Tchi {
        $tchi = Tchi::where('slug', $slug)
            ->where('active', 1)
            ->first() ;
                
        if(is_null($tchi)) {
            throw new \Exception("Aucun Tchi associé à ce slug") ;
        } else {
            return $tchi ;
        }
    }
    
    //récupérer tous les Tchis activés ; les activés + désactivés seront accessibles via Tchi::all()
    static function lireTousTchis():array {
        $tchis = Tchi::where('active', 1)
            ->find() ;
        
        return $tchis ;
    }
    
    
/* --------------------Méthodes persos ----------------------------- */
    
    public function desactiver() {
            //effacer stats
            TchiStats::where('id_tchi', $this->id)
                ->delete();
            
            //désactiver Tchi
            $this->update(['active' => 0]) ;
        
    }
            
    //TODO gestion des stats désactivées pour le mode vacances
    public function recupStats():TchiStats {
        if($this->active === 1) {
            $stats = TchiStats::where('id_tchi', $this->id)->first() ;
            return $stats ;
        } else {
            throw new \Exception("Ce Tchi est désactivé et ne peut donc avoir de stats") ;
        }     
    }
    
    
    public function vieillir() {
        $newAge = $this->nb_jours_jeu + 1 ;
        $this->update(['nb_jours_jeu' => $newAge]) ;
        $this->recupStats()->jouer() ;
    }
}


