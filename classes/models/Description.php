<?php

namespace Models ;
 
use Illuminate\Database\Eloquent\Model ;

use \Core\SecurityFunctions ;

class Description extends Model {
    /**
     * Table associated with the model
     * @var string
     */
    protected $table = 'descriptions';
    
    
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = ["txt_membre", "txt_tchi", "fichier_css", "date_desactive", "active"] ;
    
    public $timestamps = false ;
    
    public function membre() {
        return $this->belongsTo('Models\Membre', 'id_membre');
    }
    
    
/* --------------------Méthodes CRUD+ ----------------------------- */
    
    static function creerDescription(string $txtMembre, string $txtTchi, string $css, Membre $membre):Description {
        $description = new Description ;
        
        $description->id_membre = $membre->id ;
        $description->txt_membre = $txtMembre ;
        $description->txt_tchi = $txtTchi ;
        
        $nomFichier = self::enregistrerCSS($css, $membre->slug) ;
        
        $description->fichier_css = $nomFichier ;
        
        $description->save() ;
        
        return $description ;
    }
    


/* --------------------Méthodes persos ----------------------------- */
    
    public function desactiver() {
        $this->update([
                'active' => 0,
                'date_desactive' => date("Y-m-d H:i:s")
            ]) ;
    }
    
    static function enregistrerCSS(string $css, string $slugMembre):string {
        //TODO Rectifier la concaténation avec la manière sécurisée
        $nomFichier = $slugMembre.SecurityFunctions::genererSuffixe() ;
        $chemin = __DIR__ . '/../../public/css/profils/'.$nomFichier.'.css' ;
        fopen($chemin, 'w+');
        
        file_put_contents($chemin, $css);
        
        return $nomFichier ;
    }
}