<?php

namespace Models ;
 
use Illuminate\Database\Eloquent\Model ;

class TchiStats extends Model {
    /**
     * Table associated with the model
     * @var string
     */
    protected $table = 'tchisstats';
    
    
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = ["satiete", "repos", "proprete", "distraction", "date_dernieremaj", "active"] ;

    public $timestamps = false ;



   
    
/* --------------------Méthodes CRUD+ ----------------------------- */
    
    static function creerStats(Tchi $tchi):TchiStats {
        $stats = new TchiStats ;
        
        $stats->id_tchi = $tchi->id ;
        $stats->satiete = 80 ;
        $stats->repos = 50 ;
        $stats->proprete = 50 ;
        $stats->distraction = 20 ;
                
        $stats->save();
        
        return $stats ;
    }
    
    
/* --------------------Méthodes persos ----------------------------- */
    
    public function nourrir() {
        $this->satiete = $this->ajouterEtReguler($this->satiete, 1.5) ;
        $this->proprete = $this->retirerEtReguler($this->proprete, 0.08) ;
                
        $this->save() ;
    }
    
    public function border() {
        $this->repos = $this->ajouterEtReguler($this->repos, 0.2) ;
        $this->satiete = $this->retirerEtReguler($this->satiete, 0.125) ;
        $this->proprete = $this->retirerEtReguler($this->proprete, 0.05) ;
        
        $this->save() ;
    }
    
    public function laver() {
        $this->proprete = $this->ajouterEtReguler($this->proprete, 5) ;
        $this->distraction = $this->retirerEtReguler($this->distraction, 0.25) ;
        
        $this->save() ;
    }
    
    public function jouer() {
        $this->distraction = $this->ajouterEtReguler($this->distraction, 1.125) ;
        $this->satiete = $this->retirerEtReguler($this->satiete, 0.28) ;
        $this->repos = $this->retirerEtReguler($this->repos, 0.39) ;
        $this->proprete = $this->retirerEtReguler($this->proprete, 0.22) ;
        
        $this->save() ;
    }
    
    public function desactiver() {
        $this->update(['active' => 0]) ;
    }
    
    protected function ajouterEtReguler(float $valActuelle, float $valAjout):float {
        $somme = $valActuelle + $valAjout ;
        return $somme <= 100 ? $somme : 100 ;
    }
    
    protected function retirerEtReguler(float $valActuelle, float $valARetirer):float {
        $retrait = $valActuelle - $valARetirer ;
        return $retrait >= 0 ? $retrait : 0 ;
    }
}