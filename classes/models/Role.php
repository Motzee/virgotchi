<?php

namespace Models ;
 
use Illuminate\Database\Eloquent\Model ;
 
class Role extends Model {
    protected $table = 'roles';
    protected $fillable = [] ;
    
    public function membres() {
        return $this->belongsToMany('Models\Membre', 'role_membre', 'id_role', 'id_membre');
    }
    
    public $timestamps = false ;
}