<?php

namespace Controllers ;

use \Slim\Http\Request as Request;
use \Slim\Http\Response as Response;

class ApiController extends BaseController {
    
    public function readTchi(Request $request, Response $response, array $args) {
        
        if(is_null($this->tchi)) {
            $data = [
            "errors" => 1,
            "msg" => ["aucun Tchi é__è..."]
        ] ;
        } else {
            $data = [
                "errors" => 0,
                "values" => [
                    "nom" => $this->tchi->nom,
                    "nbJours" => $this->tchi->nbJoursJeu
                ]
            ] ;
        }
        
        return $response->withJson($data);
    }
  
    public function readStats(Request $request, Response $response, array $args) {
    
        if(is_null($this->tchi) || is_null($this->tchistats)) {
            $data = [
            "errors" => 1,
            "msg" => ["aucun Tchi ou absence de stats é__è..."]
        ] ;
        } else {
            $data = [
                "errors" => 0,
                "values" => [
                    "satiete" => $this->tchistats->satiete,
                    "repos" => $this->tchistats->repos,
                    "proprete" => $this->tchistats->proprete,
                    "distraction" => $this->tchistats->distraction
                ]
            ] ;
        }
        
        return $response->withJson($data);
    }
    
    public function updateStats(Request $request, Response $response, array $args) {
    
        $data = $request->getParsedBody();
        
        $authorizedActions = ["btnNourrir", "btnBorder", "btnToiletter", "btnAmuser"] ;
        
        if(isset($data["action"]) && in_array($data["action"], $authorizedActions)) {
            
            switch($data["action"]) {
                case "btnNourrir" :
                    $this->tchistats->nourrir() ;
                    break ;
                case "btnBorder" :
                    $this->tchistats->border() ;
                    break ;
                case "btnToiletter" :
                    $this->tchistats->laver() ;
                    break ;
                case "btnAmuser" :
                    $this->tchistats->jouer() ;
                    break ;
                default :
                    return $response->withJson([
                        "errors" => 1,
                        "msg" => ["L'action semble à la fois exister et ne pas exister, c'est très curieux... Prévenez l'admin : un portail dimensionnel s'est probablement ouvert sur le serveur !"]
                    ]);
            }
            
            $dataToSend = [
                "errors" => 0,
                "values" => [
                    "satiete" => $this->tchistats->satiete,
                    "repos" => $this->tchistats->repos,
                    "proprete" => $this->tchistats->proprete,
                    "distraction" => $this->tchistats->distraction
                ]
            ] ;

            return $response->withJson($dataToSend);
            
        } else {
            return $response->withJson([
                "errors" => 1,
                "msg" => ["Action non reconnue"]
            ]);
        }
    }
}
