<?php

namespace Controllers ;

use \Slim\Http\Request as Request;
use \Slim\Http\Response as Response;

use \Models\Description ;
use \Models\Membre ;
use \Models\Tchi ;
use \Models\TchiStats ;
use \Core\FieldsForms ;

class TraitementController extends BaseController {
    
    public function log(Request $request, Response $response, array $args) {
        $data = $request->getParsedBody();
        
        $filteredData = $this->filterParamsReceived($data, FieldsForms::LOG_FORM) ;
        
        switch($filteredData["logtype"]) {
            case 'connexion' :
                $reussiteConnexion = $this->essaiConnexion($filteredData["pseudo"], $filteredData["password"]) ;
                if($reussiteConnexion !== true) {
                    return $this->returnErrorPage($response, $reussiteConnexion) ;
                } 
                break ;
            case 'inscription' :
                $reussiteInscription = $this->essaiInscription($filteredData["pseudo"], $filteredData["password"]) ;
                if($reussiteInscription !== true) {
                    return $this->returnErrorPage($response, $reussiteInscription) ;
                }
                break ;
            default :
                return $this->returnErrorPage($response, [
                    "msg" => "Action inconnue...",
                    "location" => "/",
                    "location_name" => "l'accueil du site"
                ]) ;
        }
        return $response->withRedirect($this->router->pathFor('index'));
    }
   
    
    public function updateCompte(Request $request, Response $response, array $args) {
        $data = $request->getParsedBody();
        
        $filteredData = $this->filterParamsReceived($data, FieldsForms::COMPTE_UPDATE_FORM) ;
        
        if(password_verify($filteredData["passwd"], $this->membre->passhash)) {
            if($filteredData["modifierPass"] === true) {
                $passhash = password_hash($filteredData["new_passwd"], PASSWORD_DEFAULT) ;
                $this->membre->modifierMembre($filteredData["email"], $passhash) ;
            } else {
                $this->membre->modifierMembre($filteredData["email"], null) ;
            }
            return $response->withRedirect($this->router->pathFor('compteParametrer'));
        } else {
            return $this->returnErrorPage($response, [
                "msg" => "Le mot de passe utilisé n'est pas le bon",
                "location" => "/",
                "location_name" => "l'accueil du site"
            ]) ;
        }
    }
    
    public function deleteCompte(Request $request, Response $response, array $args) {
        $data = $request->getParsedBody();
        
        $filteredData = $this->filterParamsReceived($data, FieldsForms::COMPTE_DELETE_FORM) ;
        
        if($filteredData["modifierIrreversible"] === true) {
            switch($filteredData["actionDelete"]) {
                case "relacherTchi" :
                    if(!is_null($this->tchi)) {
                        $this->tchi->desactiver() ;
                    }
                    return $response->withRedirect($this->router->pathFor('tchi'));
                case "supprimerCompte" :
                    //Désactivation du compte, description, tchi et stats
                    $this->membre->desactiver() ;
                    
                    //Déconnexion et donc redirection
                    $this->membre->deconnexion() ;
                    return $response->withRedirect($this->router->pathFor('index'));
                default :
                    return $this->returnErrorPage($response, [
                        "msg" => "Action non-libellée",
                        "location" => "/compte",
                        "location_name" => "la page Paramétrer mon compte"
                    ]) ;
            }
        } else {
            return $this->returnErrorPage($response, [
                "msg" => "Vous n'avez pas coché la case qui indique que cette suppression est définitive, cette suppression n'a donc pas eu lieu. Cochez la case si vous êtes vraiment sûr·e de votre choix.",
                "location" => "/compte",
                "location_name" => "la page Paramétrer mon compte"
            ]) ;
        }
    }
    
    
    public function createTchi(Request $request, Response $response, array $args) {
    
        $data = $request->getParsedBody();
        
        $filteredData = $this->filterParamsReceived($data, FieldsForms::TCHI_CREATE_FORM) ;
        
        //création d'un tchi
        Tchi::creerTchi($filteredData["nomTchi"], $this->membre->id) ;
        
        return $response->withRedirect($this->router->pathFor('tchi'));
    }
    
    
    public function postProfil(Request $request, Response $response, array $args) {
        $data = $request->getParsedBody();
        
        $filteredData = $this->filterParamsReceived($data, FieldsForms::PROFIL_FORM) ;
        
        //desactiver l'éventuelle ancienne
        if(!is_null($this->membre->recupDescription())) {
            $this->membre->recupDescription()->desactiver() ;
        }
        
        //créer la nouvelle
        Description::creerDescription($filteredData["descriptionMembre"], $filteredData["descriptionTchi"], $filteredData["miseEnFormeCss"], $this->membre) ;
        
        //redirection finale
        return $response->withRedirect($this->router->pathFor('profilPersonnaliser'));
    }

/* Bibliothèque de fonctions internes */

    protected function essaiConnexion(string $pseudo, string $pass) {
        $membreDemande = Membre::where([
                "pseudo" => $pseudo,
                "active" => 1
            ])->first() ;
        if(!is_null($membreDemande) && password_verify($pass, $membreDemande->passhash)) {
            $membreDemande->connexion() ;
            return true ;
        } else {
            return [
                "msg" => "Erreur de pseudo et/ou de mot de passe",
                "location" => "/",
                "location_name" => "l'accueil du site"
            ] ;
        }

    }
    
    protected function essaiInscription(string $pseudo, string $pass) {
        if(!preg_match("/^[a-zA-Z0-9-_]{2,32}$/", $pseudo)) {
            return [
                "msg" => "Un caractère non-autorisé a été utilisé pour le pseudo. Veuillez n'employer que des chiffres, lettre majuscules ou minuscules ou tirets (- ou _)... Ou alors, le pseudo est trop long (plus de 32 caractères ?)",
                "location" => "/",
                "location_name" => "l'accueil du site"
            ] ;
        }
        
        if(Membre::existeMembre(\Core\SecurityFunctions::genererSlug($pseudo)) === true) {
            return [
                "msg" => "Echec de l'inscription : ce pseudo est déjà utilisé, ou bien réservé",
                "location" => "/",
                "location_name" => "l'accueil du site"
            ] ;
        } else {
            $passhash = password_hash($pass, PASSWORD_DEFAULT);
            
            //création du membre
            $membre = Membre::creerMembre($pseudo, $passhash) ;
            
            $membre->connexion() ;
            
            return true ;
        }
    }
}