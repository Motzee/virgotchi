<?php

namespace Controllers;

use \Slim\Http\Request as Request;
use \Slim\Http\Response as Response;

use \Core\SecurityFunctions ;
use \Models\Membre ;
use \Models\Tchi ;
use \Models\TchiStats ;

class BaseController {

    protected $container ;
    protected $membre ;
    protected $tchi ;
    protected $tchistats ;

    public function __construct($container) {

        $this->container = $container;
        
        if(isset($_SESSION['slug'])) {
            $this->membre = Membre::where("slug", $_SESSION['slug'])
                ->where("active", 1)
                ->first() ;
            
            if(is_null($this->membre)) {
                throw new \Exception("Vous semblez ne pas exister, veuillez contacter l'admin") ;
            } else {
                $this->tchi = $this->membre->recupTchi() ;
                if(!is_null($this->tchi) && $this->tchi->active === 1) {
                    $this->tchistats = $this->tchi->recupStats() ;
                } else {
                    $this->tchistats = null ;
                }
            }
        } else {
            return false ;
        }
    }

    public function __get($property) {
        if ($this->container->{$property}) {
            return $this->container->{$property};
        }
    }

    public function prepareAntiCSRF(Request $request): array {
        
        $nameKey = $this->csrf->getTokenNameKey();
        $valueKey = $this->csrf->getTokenValueKey();
        $name = $request->getAttribute($nameKey);
        $value = $request->getAttribute($valueKey);

        return [
            "name" => $name,
            "nameKey" => $nameKey,
            "value" => $value,
            "valueKey" => $valueKey
        ];
    }

    public function filterParamsReceived(array $dataReceived, array $dataExpected):array {
        $filteredData = [];

        foreach ($dataExpected as $keyName => $expectedParams) {
            if (isset($dataReceived[$keyName])) {
                switch ($expectedParams["type"]) {
                    case 'string' :
                        $filteredData[$keyName] = SecurityFunctions::checkString($dataReceived[$keyName], $expectedParams);
                        break ;
                    case 'checkbox' :
                        $filteredData[$keyName] = true ;
                        break ;
                    default :
                        throw new \Exception('One or more field(s) with unidentified type(s)');
                }
            } else {
                if($expectedParams["type"] === "checkbox") {
                    $filteredData[$keyName] = false ;
                } else {
                    throw new \Exception('Expected field missing');
                }
            }
        }
        return $filteredData;
    }

    public function returnErrorPage(Response $response, array $error) {
        return $this->view->render($response, 'pages/errors/error.twig', $error);
    }
}
