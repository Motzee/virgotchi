<?php

namespace Controllers ;

use \Slim\Http\Request as Request;
use \Slim\Http\Response as Response;

use \Models\Membre ;
use \Models\Tchi ;

class NavigationController extends BaseController {
    
    protected $paramsForTwig = [] ;
    
    function __construct($container) {
        parent::__construct($container);
        
        if(!is_null($this->membre)) {
            $this->paramsForTwig["membre"] = [
                "pseudo" => $this->membre->pseudo,
                "roles" => $this->membre->recupRolesSlugs()
            ] ;
        }
    }
    
    
/* Visiteurs */ 
    public function index(Request $request, Response $response) {
        $this->paramsForTwig["antiCsrf"] = $this->prepareAntiCSRF($request) ;

        return $this->view->render($response, 'pages/index.twig', $this->paramsForTwig);
    }
    
    public function resume(Request $request, Response $response) {
        $this->paramsForTwig["antiCsrf"] = $this->prepareAntiCSRF($request) ;

        return $this->view->render($response, 'pages/resume.twig', $this->paramsForTwig);
    }
    
    public function infosLegales(Request $request, Response $response) {
        $this->paramsForTwig["antiCsrf"] = $this->prepareAntiCSRF($request) ;

        return $this->view->render($response, 'pages/infoslegales.twig', $this->paramsForTwig);
    }
    
    public function profilVoir(Request $request, Response $response, array $args) {
        $this->paramsForTwig["antiCsrf"] = $this->prepareAntiCSRF($request) ;

        $membreToShow = Membre::where("slug", $args["slug"])
            ->where('active', 1)
            ->first() ;
        
        if(is_null($membreToShow)) {
            return $this->returnErrorPage($response, [
                "msg" => "Aucun membre ne possède un pseudo associé à ce slug.",
                "location" => "/",
                "location_name" => "l'accueil du site"
            ]) ;
        }
        
        $tchiToShow = $membreToShow->recupTchi() ;
        $fiche = $membreToShow->recupDescription() ;
        
        $this->paramsForTwig["fiche"]["pseudo"] = $membreToShow->pseudo ;
        
        if(!is_null($tchiToShow)) {
            $this->paramsForTwig["fiche"]["nomTchi"] = $tchiToShow->nom ;
            $this->paramsForTwig["fiche"]["nbJoursTchi"] = $tchiToShow->nb_jours_jeu ;
            $this->paramsForTwig["fiche"]["descriptionTchi"] = is_null($fiche) ? "" : $fiche->txt_tchi ;
        }
        
        if(!is_null($fiche)) {
            $this->paramsForTwig["fiche"]["descriptionMembre"] = $fiche->txt_membre ;
            $this->paramsForTwig["fiche"]["ficheCSS"] = $fiche->fichier_css ;
        }

        return $this->view->render($response, 'pages/profilVoir.twig', $this->paramsForTwig);
    }

    public function errorPage(Request $request, Response $response, array $args) {
        $this->returnErrorPage($response, [
            "msg" => "Tentative d'accès à une zone réservée. Tu n'es peut-être pas identifié·e ?",
            "location" => "/",
            "location_name" => "l'accueil du site"
        ]) ;
    }
    
    
/* Membres connectés */    
    public function tchi(Request $request, Response $response) {
        $this->paramsForTwig["antiCsrf"] = $this->prepareAntiCSRF($request) ;
        
        if(!is_null($this->tchi)) {
            //viellir le tchi si première action de la journée
            $dateDerniereAction = explode(" ", $this->tchistats->date_dernieremaj) ;
            if($dateDerniereAction[0] !== date('Y-m-d')) {
                $this->tchi->vieillir() ;
            }

            $this->paramsForTwig["tchi"] = [
                "nomTchi" => $this->tchi->nom,
                "nbJours" => $this->tchi->nb_jours_jeu
            ] ;
        }
        
        return $this->view->render($response, 'pages/tchi.twig', $this->paramsForTwig);
    }
    
    public function profilPersonnaliser(Request $request, Response $response) {
        $this->paramsForTwig["antiCsrf"] = $this->prepareAntiCSRF($request) ;
        
        $fiche = $this->membre->recupDescription() ;

        if(!is_null($fiche)) {
            //TODO concaténation sécurisée
            $chemin = __DIR__ . '/../../public/css/profils/'.$fiche->fichier_css.'.css' ;
        
            $contentCSS = file_get_contents($chemin);
            
            $this->paramsForTwig["fiche"] = [
                "slug" => $this->membre->slug,
                "descriptionTchi" => $fiche->txt_tchi,
                "descriptionMembre" => $fiche->txt_membre,
                "ficheCSS" => $contentCSS
            ] ;
        } else {
            $this->paramsForTwig["fiche"] = [
                "slug" => $this->membre->slug
            ] ;
        }
        
        return $this->view->render($response, 'pages/profilPersonnaliser.twig', $this->paramsForTwig);
    }
    
    public function compteParametrer(Request $request, Response $response) {
        $this->paramsForTwig["antiCsrf"] = $this->prepareAntiCSRF($request) ;
        
        $this->paramsForTwig["membre"]["inscription"] = $this->membre->date_inscript ;
        $this->paramsForTwig["membre"]["anciennete"] = $this->membre->nb_jours_jeu ;
        $this->paramsForTwig["membre"]["email"] = $this->membre->email ;
        
        return $this->view->render($response, 'pages/compteParametrer.twig', $this->paramsForTwig);
    }
    
    public function deconnexion(Request $request, Response $response, array $args) {
        $this->membre->deconnexion() ;
        return $response->withRedirect($this->router->pathFor('index'));
    }
    
    
/* Admins */ 
    public function administration(Request $request, Response $response) {
        $this->paramsForTwig["antiCsrf"] = $this->prepareAntiCSRF($request) ;

        $nbMembres = Membre::count() ;
        $nbTchis = Tchi::count() ;
        
        $this->paramsForTwig["isAdmin"] = true ;
                
        $this->paramsForTwig["nbElements"] = [
            "membres" => $nbMembres,
            "tchis" => $nbTchis
        ] ;
        
        return $this->view->render($response, 'pages/administration.twig', $this->paramsForTwig);
    }
    
}