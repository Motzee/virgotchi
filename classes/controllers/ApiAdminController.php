<?php

namespace Controllers ;

use \Slim\Http\Request as Request;
use \Slim\Http\Response as Response;

use \Models\Membre ;
use \Models\Tchi ;

class ApiAdminController extends BaseController {
    
    public function allMembres(Request $request, Response $response, array $args) {
        
        $nbMembres = Membre::count() ;
        
        $data = [
            "errors" => 0,
            "nbValues" => $nbMembres,
            "values" => []
        ] ;
        
        $membres = Membre::all() ;
        
        foreach ($membres as $aMembre) {
            $data["values"][] = [
                "pseudo" => $aMembre->pseudo,
                "slug" => $aMembre->slug,
                "derniereco" => $aMembre->date_derniereco->format('d-m-Y'),
                "active" => $aMembre->active
            ] ;
        }
        
        return $response->withJson($data);
    }
    
    public function allTchis(Request $request, Response $response, array $args) {
        
        $nbTchis = Tchi::count() ;
        
        $data = [
            "errors" => 0,
            "nbValues" => $nbTchis,
            "values" => []
        ] ;
        
        $tchis = Tchi::all() ;
        
        foreach ($tchis as $aTchi) {
            $data["values"][] = [
                "nomTchi" => $aTchi->nom,
                "pseudo" => $aTchi->membre->pseudo,
                "slugPseudo" => $aTchi->membre->slug,
                "nbJours" => $aTchi->nb_jours_jeu,
                "active" => $aTchi->active
            ] ;
        }
        
        return $response->withJson($data);
    }
}