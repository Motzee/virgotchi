<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use \Core\SecurityFunctions ;

final class SecurityFunctionsTest extends TestCase
{
    public function testgenererSuffixe(): void {

        $testValeur = SecurityFunctions::genererSuffixe() ;

        /*doit 
        - renvoyer une chaine de caractères
        - commencer par _
        - tailler entre 20 et 64
        */

        $typeAttendu = "string" ;
        $this->assertInternalType($typeAttendu, $testValeur, "Ne renvoie pas le type attendu : ".$typeAttendu) ;

        $debut = "_" ;
        $this->assertStringStartsWith($debut, $testValeur, "Ne commence pas par : ".$debut) ;

        $tailleAttendue = 64 ;
        $this->assertLessThan($tailleAttendue, strlen($testValeur), "Ne semble pas assez petit : mesure ".strlen($testValeur)." au lieu de ".$tailleAttendue) ;

        $tailleAttendue = 20 ;
        $this->assertGreaterThan($tailleAttendue, strlen($testValeur), "Ne semble pas assez grand : mesure ".strlen($testValeur)." au lieu de ".$tailleAttendue) ;
    }

    public function testGenererSlug(): void {

        $testValeur = SecurityFunctions::genererSlug("Manon26", true) ;

        /* doit :
        - renvoyer une chaine de caractères
        - commencer par un truc précis
        - ne pas dépasser 128 caractères
        */

        $typeAttendu = "string" ;
        $this->assertInternalType($typeAttendu, $testValeur, "Ne renvoie pas le type attendu : ".$typeAttendu) ;

        $debut = "manon26_" ;
        $this->assertStringStartsWith($debut, $testValeur, "Ne commence pas par : ".$debut) ;

        $tailleAttendue = 128 ;
        $this->assertLessThan($tailleAttendue, strlen($testValeur), "Ne semble pas assez petit : mesure ".strlen($testValeur)." au lieu de ".$tailleAttendue) ;

        $tailleAttendue = 22 ;
        $this->assertGreaterThan($tailleAttendue, strlen($testValeur), "Ne semble pas assez grand : mesure ".strlen($testValeur)." au lieu de ".$tailleAttendue) ;

    }
}
