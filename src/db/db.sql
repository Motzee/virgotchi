CREATE DATABASE if not exists virgotchi character set utf8 collate utf8_unicode_ci ;

GRANT ALL PRIVILEGES ON virgotchi.* TO 'UsernameSQL'@'localhost' ;

USE virgotchi ;

/* ----------------------------------------------------------------- */
/* Création des tables */

DROP TABLE if exists roles ;

CREATE TABLE roles (
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    nom VARCHAR(32) NOT NULL UNIQUE,
    slug VARCHAR(64) NOT NULL UNIQUE
) engine=innodb character set utf8 collate utf8_unicode_ci ;

/* ----------------------- */

DROP TABLE if exists membres ;

CREATE TABLE membres (
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    pseudo VARCHAR(32) NOT NULL UNIQUE,
    slug VARCHAR(64) NOT NULL UNIQUE,
    email VARCHAR(255),
    passhash VARCHAR(255) NOT NULL,
    date_inscript TIMESTAMP NOT NULL,
    date_derniereco TIMESTAMP NOT NULL,
    nb_jours_jeu SMALLINT NOT NULL DEFAULT 1,
    active BOOL NOT NULL DEFAULT 1
) engine=innodb character set utf8 collate utf8_unicode_ci ;

/* ----------------------- */

DROP TABLE if exists role_membre ;

CREATE TABLE role_membre (
    id_role INT NOT NULL,
    id_membre INT NOT NULL,
    FOREIGN KEY (id_role) REFERENCES roles(id),
    FOREIGN KEY (id_membre) REFERENCES membres(id)
) engine=innodb character set utf8 collate utf8_unicode_ci ;

/* ----------------------- */

DROP TABLE if exists descriptions ;

CREATE TABLE descriptions (
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    id_membre INT NOT NULL,
    date_creation TIMESTAMP NOT NULL,
    txt_membre TEXT,
    txt_tchi TEXT,
    fichier_css VARCHAR(55),
    date_desactive TIMESTAMP,
    active BOOL NOT NULL DEFAULT 1,
    FOREIGN KEY (id_membre) REFERENCES membres(id)
) engine=innodb character set utf8 collate utf8_unicode_ci ;

/* ----------------------- */

DROP TABLE if exists tchis ;

CREATE TABLE tchis (
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    nom VARCHAR(64) NOT NULL,
    slug VARCHAR(128) NOT NULL UNIQUE,
    id_membre INT NOT NULL,
    date_instance TIMESTAMP NOT NULL,
    nb_jours_jeu SMALLINT NOT NULL DEFAULT 1,
    active BOOL NOT NULL DEFAULT 1,
    FOREIGN KEY (id_membre) REFERENCES membres(id)
) engine=innodb character set utf8 collate utf8_unicode_ci ;

/* ----------------------- */

DROP TABLE if exists tchisstats ;

CREATE TABLE tchisstats (
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    id_tchi INT NOT NULL,
    satiete SMALLINT(150) NOT NULL,
    repos SMALLINT(150) NOT NULL,
    proprete SMALLINT(150) NOT NULL,
    distraction SMALLINT(150) NOT NULL,
    active BOOL NOT NULL DEFAULT 1,
    FOREIGN KEY (id_tchi) REFERENCES tchis(id)
) engine=innodb character set utf8 collate utf8_unicode_ci ;