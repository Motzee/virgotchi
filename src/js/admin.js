import {ajaxMultisurfaces} from "./paquets/ajaxMultisurfaces" ;

if(document.getElementById("pageAdministration") !== null) {
    
    var actionsApresReqAjax = {} ;

    actionsApresReqAjax["adminMembres"] = function(data) {
        remplirTabMembres("membresTbody", data);
    } ;

    actionsApresReqAjax["adminTchis"] = function(data) {
        remplirTabTchis("tchisTbody", data) ;
    } ;

    let zones = ["administrerMembres", "administrerTchis"] ;


    ajaxMultisurfaces({
        url: '/admin/api/membres',
        callback: actionsApresReqAjax["adminMembres"]
    }) ;


    let actionAdminMembres = document.getElementById("actionAdminMembres") ;
    actionAdminMembres.addEventListener('click', function() {
        ajaxMultisurfaces({
            url: '/admin/api/membres',
            callback: actionsApresReqAjax["adminMembres"]
        }) ;
        montrerZone("administrerMembres", zones) ;
    }) ;

    let actionAdminTchis = document.getElementById("actionAdminTchis") ;
    actionAdminTchis.addEventListener('click', function() {
        ajaxMultisurfaces({
            url: '/admin/api/tchis',
            callback: actionsApresReqAjax["adminTchis"]
        }) ;
        montrerZone("administrerTchis", zones) ;
    }) ;

}

function remplirTabMembres(idTableau, data) {
        let tableau = document.getElementById(idTableau) ;
        tableau.textContent = "" ;
        for(let entree in data.values) {
            let ligne = document.createElement("tr");
            let lienMembre = document.createElement("a");
            let cellulePseudo = document.createElement("td");
            lienMembre.textContent = data.values[entree]["pseudo"] ;
            lienMembre.href = "/profil/"+data.values[entree]["slug"] ;
            cellulePseudo.appendChild(lienMembre) ;
            ligne.appendChild(cellulePseudo) ;

            creerElement("td", data.values[entree]["derniereco"], ligne) ;
            creerElement("td", data.values[entree]["active"], ligne) ;
            tableau.appendChild(ligne);
        }
        let nbResults = document.getElementById("nbMembres") ;
        nbResults.textContent = data.nbValues ;
}

    
function remplirTabTchis(idTableau, data) {
        let tableau = document.getElementById(idTableau) ;
        tableau.textContent = "" ;
        for(let entree in data.values) {
            let ligne = document.createElement("tr");
            creerElement("td", data.values[entree]["nomTchi"], ligne) ;

            let lienMembre = document.createElement("a");
            let cellulePseudo = document.createElement("td");
            lienMembre.textContent = data.values[entree]["pseudo"] ;
            lienMembre.href = "/profil/"+data.values[entree]["slugPseudo"] ;
            cellulePseudo.appendChild(lienMembre) ;
            ligne.appendChild(cellulePseudo) ;

            creerElement("td", data.values[entree]["nbJours"], ligne) ;
            creerElement("td", data.values[entree]["active"], ligne) ;
            tableau.appendChild(ligne);
        }
        let nbResults = document.getElementById("nbTchis") ;
        nbResults.textContent = data.nbValues ;
}

function creerElement(elementHtml, txt, parent) {
        let element = document.createElement(elementHtml);
        element.textContent = txt;
        parent.appendChild(element);
}


function montrerZone(idZone, listeZones) {
        for(let aZone in listeZones) {
            document.getElementById(listeZones[aZone]).classList.add("hidden");
        }
        document.getElementById(idZone).classList.remove("hidden");
}
