import {ajaxMultisurfaces} from "./ajaxMultisurfaces" ;
import {updateBarresStats} from "./tchi" ;

export function indexSite() {
    let authForm = document.getElementById("authForm") ;
    if(typeof authForm !== 'undefined' && authForm !== null) {
        authForm.addEventListener("submit", function(e){
            
            let pseudo = document.getElementById("pseudo").value ;

            if (/^[a-zA-Z0-9_-]{2,32}$/.test(pseudo) === false) {
                e.preventDefault();
                alert("Votre pseudo ne peut contenir que des lettres majuscules ou minuscules, des chiffres ou des tirets - ou _");
            }
        }, false) ;
    }
}

export function menuNavigation() {
   
    let btnOpenMenuNav = document.getElementById("btnMenuNav") ;
    
    if(typeof btnOpenMenuNav !== 'undefined' && btnOpenMenuNav !== null) {

        let modaleMenuNav = document.getElementById("modaleMenuNav") ;

        btnOpenMenuNav.addEventListener("click", function() {
            modaleMenuNav.classList.remove("hiddenModale");
        }) ;
	        
        let btnCloseMenuNav = document.getElementById("closeModale") ;
        btnCloseMenuNav.addEventListener("click", function() {
            modaleMenuNav.classList.add("hiddenModale");
        }) ;
    }
        
}

export function parametrageCompte() {
	let champNewPasswd = document.getElementById("champNewPasswd") ;

	let caseModifierPass = document.getElementById("modifierPass") ;
	caseModifierPass.addEventListener("click", function() {
	    champNewPasswd.classList.toggle("hidden");
	}) ;

	let actionDelete = document.getElementById("actionDelete") ;

	let btnRelacherTchi = document.getElementById("relacherTchi") ;
	btnRelacherTchi.addEventListener("click", function(event) {
	    actionDelete.value = "relacherTchi" ;
	}) ;

	let btnSupprimerCompte = document.getElementById("supprimerCompte") ;
	btnSupprimerCompte.addEventListener("click", function(event) {
	    actionDelete.value = "supprimerCompte" ;
	}) ;
}

export function affichageTchi() {

	ajaxMultisurfaces({
        url: '/api/stats',
        callback: updateBarresStats
    }) ;
    
    let btnsInteractionTchi = [
        "btnNourrir",
        "btnBorder",
        "btnToiletter",
        "btnAmuser"
    ] ;
    
    for(let aBtn in btnsInteractionTchi) {
        let csrf_name = document.getElementsByName("csrf_name")[0].value ;
        let csrf_value = document.getElementsByName("csrf_value")[0].value ;
        
        let btn = document.getElementById(btnsInteractionTchi[aBtn]) ;
        btn.addEventListener("click", function() {
        	
        	let argumnts = "_METHOD=PUT&action="+btnsInteractionTchi[aBtn]+"&csrf_name="+csrf_name+"&csrf_value="+csrf_value ;

        	ajaxMultisurfaces({
		        method: 'POST',
		        url: '/api/stats',
		        args: argumnts,
		        callback: updateBarresStats
		    }) ;
        }) ;
    }

}

//requête ajax
function ajaxRequest(reqType, url, actionKey, data = null) {
    let authorizedReqType = ["GET", "POST"] ;
    
    //vérifications
    if(authorizedReqType.indexOf(reqType) === -1) {
        console.log("Method request not authorized") ;
    }
    
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            data = JSON.parse(this.response, true) ;
            if(data.errors === 0) {
                actionsApresReqAjax[actionKey](data) ;
            } else {
                console.log("Le back renvoie une erreur !") ;
            }
        }
    };
    xhr.open(reqType, url, true);
    if(data !== null) {
        xhr.send(data);
    } else {
        xhr.send();
    }
}