export function updateBarresStats(newStats) {

    //tableau associant chaque champ reçu en ajax avec l'id de la barre à remplir avec
    let idsBarres = {
        "satiete" : "statSatiete",
        "repos" : "statRepos",
        "proprete" : "statProprete",
        "distraction" : "statDistraction"
    } ;
    
    let values = newStats.values ;

    for(let aStat in values) {
        let barreValue = values[aStat] * 2.5 ;
        let barre = document.getElementById(idsBarres[aStat]) ;
        barre.style.backgroundImage = "linear-gradient(to right, grey, grey " + barreValue + "px, white " + barreValue + "px, white)" ;
    }
}
    