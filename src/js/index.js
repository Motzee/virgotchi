import {menuNavigation, parametrageCompte, affichageTchi, indexSite} from './paquets/navigation' ;

/* Déclaration du service worker si option disponible*/
if ("serviceWorker" in navigator) {
    navigator.serviceWorker
      .register("/service-worker.js")
      .then(registration => {
        console.log(
          "App: Achievement unlocked."
        );
      })
      .catch(error => {
        console.error(
          "App: Crash of Service Worker",
          error
        );
      });
  }

/* Menu de navigation */
menuNavigation() ;

let pageScript = {} ;

pageScript["pageTchi"] = affichageTchi ;
pageScript["pageProfil"] = function() {} ;
pageScript["pageProfilVoir"] = function() {} ;
pageScript["pageIndex"] = indexSite ;
pageScript["pageParametres"] = parametrageCompte ;
pageScript["pageInfoslegales"] = function() {} ;


let pagesDuSite = [
    "pageTchi",
    /*"pageProfil",
    "pageProfilVoir",*/
    "pageIndex",
    "pageParametres",
    //"pageInfoslegales"
] ;

for(let aPage in pagesDuSite) {
    if(document.getElementById(pagesDuSite[aPage]) !== null) {
        pageScript[pagesDuSite[aPage]]() ;
        break ;
    }
}