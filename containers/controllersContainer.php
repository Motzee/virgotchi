<?php

$container['NavigationController'] = function ($container) {
    return new \Controllers\NavigationController($container);
} ;

$container['TraitementController'] = function ($container) {
    return new \Controllers\TraitementController($container);
} ;

$container['ApiController'] = function ($container) {
    return new \Controllers\ApiController($container);
} ;


$container['ApiAdminController'] = function ($container) {
    return new \Controllers\ApiAdminController($container);
} ;