<?php

$container['notFoundHandler'] = function ($c) { 
    return new \Core\NotFoundHandler($c->get('view'), function ($request, $response) use ($c) { 
        return $c['response'] 
            ->withStatus(404); 
    }); 
};

$container['notAllowedHandler'] = function ($c) { 
    return new \Core\NotAllowedHandler($c->get('view'), function ($request, $response) use ($c) { 
        return $c['response'] 
            ->withStatus(401); 
    }); 
};