<?php

$container['csrf'] = function ($c) {
    //return new \Slim\Csrf\Guard; <- ancienne configuration
    /* finalement usage d'un token persistant, plus adapté à la présence d'ajax*/
    $guard = new \Slim\Csrf\Guard();
    $guard->setPersistentTokenMode(true);
    return $guard;
};

// Register middleware for all routes (if you are implementing per-route checks you must not add this)
$app->add($container->get('csrf'));

