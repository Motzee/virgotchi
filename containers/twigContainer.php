<?php

return function ($container) {
 
    $view = new \Slim\Views\Twig(__DIR__ . '/../vues', [
        /*'debug' => $container['settings']['displayErrorDetails'],*/
        'cache' => false/*$container['settings']['twigCachePath']*/
    ]);
  
    $view->addExtension(new \Slim\Views\TwigExtension(
        $container->router,
        $container->request->getUri()
    ));
    //$view->addExtension(new Twig_Extension_Debug());
  
    
    return $view;
};


